package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;


    public List<Message> getAllChatById(Long chatId) {
        List<Message> messageList = this.messageRepository.findAllByChatId(chatId);
        return messageList;
    }
    public List<Message> getTenMessagesByChatId(Long id){
        return messageRepository.getTenMessagesByChatId(id);
    }

    public List<Message> getTenMessagesByUserId(Long id){
        return messageRepository.getTenMessageByUserId(id);
    }

    public Message add(Message message){
        return this.messageRepository.save(message);
    }
    public List<Message> findByChatIdAndUserIdNot(Long chatId,Long userId){
        return messageRepository.findByChatIdAndUserIdNot(chatId,userId);
    }
    public void update(Message message){
         messageRepository.save(message);
    }



}
