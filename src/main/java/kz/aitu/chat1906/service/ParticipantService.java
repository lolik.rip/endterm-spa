package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ParticipantService {
    private final ParticipantRepository participantRepository;
    private final UserRepository userRepository;

    public ParticipantService(ParticipantRepository participantRepository, UserRepository userRepository) {
        this.participantRepository = participantRepository;
        this.userRepository = userRepository;
    }


    public List<User> getUsersByChat(Long chatId) {
        List<Participant> participantList = this.participantRepository.findAllByChatId(chatId);
        List<User> userList = new ArrayList<>();

        for (Participant participant : participantList){
            Optional<User> usersOptional = userRepository.findById(participant.getUserId());
            if (usersOptional.isPresent()){
                userList.add(usersOptional.get());
            }
        }

        return userList;
    }

    public List<User> getParticipantByUser(Long userId){
        List<Participant> participantList = this.participantRepository.findAllByUserId(userId);
        List<User> userList = new ArrayList<>();
        for (Participant participant : participantList){
            Optional<User> usersOptional = userRepository.findById(participant.getUserId());
            if (usersOptional.isPresent()){
                userList.add(usersOptional.get());
            }
        }

        return userList;
    }
}

