package kz.aitu.chat1906.service;

import kz.aitu.chat1906.exception.domain.EmailExistException;
import kz.aitu.chat1906.exception.domain.UserNotFoundException;
import kz.aitu.chat1906.model.Account;
import kz.aitu.chat1906.model.RefreshToken;
import kz.aitu.chat1906.repository.AccountRepository;
import kz.aitu.chat1906.repository.RefreshTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService {
    private final AccountRepository accountRepository;
    private final RefreshTokenRepository tokenRepository;

    @Autowired
    public AuthService(AccountRepository accountRepository, RefreshTokenRepository tokenRepository) {
        this.accountRepository = accountRepository;
        this.tokenRepository = tokenRepository;
    }

    public Account register(String email, String password){
        Optional<Account> accountOptimal = accountRepository.findAccountByEmail(email);
        if(accountOptimal.isPresent()){
            throw new EmailExistException("This email is already exist");
        } else{
            Account account = new Account();
            account.setEmail(email);
            account.setPassword(password);
            Account newAccount = accountRepository.save(account);
            RefreshToken token = new RefreshToken(UUID.randomUUID(),newAccount);
            tokenRepository.save(token);
            return newAccount;
        }
    }

    public RefreshToken authenticate(String email, String password) {
        Optional<Account> accountOptional = accountRepository.findAccountByEmailAndPassword(email,password);
        if(accountOptional.isPresent()){
            return tokenRepository.findByAccountId(accountOptional.get().getId()).get();
        } else{
            throw new UserNotFoundException("Account not found by email " + email);
        }
    }

}
