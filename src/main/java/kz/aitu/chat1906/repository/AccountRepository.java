package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findAccountByEmail(String email);
    Optional<Account> findAccountByEmailAndPassword(String email, String password);
}
