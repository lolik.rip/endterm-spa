package kz.aitu.chat1906.model;

import java.util.StringJoiner;
import java.util.UUID;

public class AccountID{
    private UUID id;

    public AccountID(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AccountID.class.getSimpleName() + "[", "]")
                .add(String.format("id=%s", id))
                .toString();
    }
}