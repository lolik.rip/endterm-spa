package kz.aitu.chat1906.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
public class RefreshToken {
    @Id
    private String id;

    private UUID uuid;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(nullable = false, updatable = false)
    private LocalDateTime createdDate;

    public RefreshToken(UUID uuid, Account account){
        this.uuid = uuid;
        this.account = account;
    }

    public RefreshToken() {
    }

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }
}
