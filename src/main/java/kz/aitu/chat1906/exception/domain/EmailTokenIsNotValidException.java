package kz.aitu.chat1906.exception.domain;

public class EmailTokenIsNotValidException extends RuntimeException {
    public EmailTokenIsNotValidException(String message) {
        super(message);
    }
}
