package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Account;
import kz.aitu.chat1906.model.RefreshToken;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.service.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Controller
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping({"/register"})
    public  ResponseEntity<Object> register(@RequestBody Account account){
        Account acc = authService.register(account.getEmail(), account.getPassword());
        if(acc!=null){
            return ResponseEntity.ok().body("Account created");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PostMapping({"/login"})
    public  ResponseEntity<Object> login(@RequestBody Account account){
        RefreshToken token = authService.authenticate(account.getEmail(), account.getPassword());
        if(token!=null){
            return ResponseEntity.ok().header("token", token.getUuid().toString()).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

}
