package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.MessageRepository;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final ParticipantRepository participantRepository;

    @GetMapping({"/chat/{id}"})
    public ResponseEntity<?> getMessagesByChatId(@PathVariable(name = "id") Long chatId) {
        return ResponseEntity.ok(messageService.getAllChatById(chatId));
    }

    @GetMapping("/getTenMessagesByChatId/{id}")
    public ResponseEntity<?> getTenMessageByChatId(@PathVariable Long id, @RequestHeader  Long userId) {
        if (!participantRepository.existsByChatIdAndUserId(id, userId)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<Message> messageList = messageService.findByChatIdAndUserIdNot(id, userId);
        Date date = new Date();

        for (Message message : messageList) {

            if (!message.isDelivered() ) {
                message.setDelivered(true);
                message.setDeliveredTimestamp(date.getTime());
                messageService.update(message);

            }


        }
        return ResponseEntity.ok(messageService.getTenMessagesByChatId(id));
    }

    @GetMapping("/getAllNotDelivered/{id}")
    public ResponseEntity<?> getAllNotDelivered(@PathVariable Long id, @RequestHeader  Long userId) {
        if (!participantRepository.existsByChatIdAndUserId(id, userId)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<Message> messageList = messageService.findByChatIdAndUserIdNot(id, userId);
        Date date = new Date();
        List<Message> notDeliveredMessage = new ArrayList<>();
        for (Message message : messageList) {

            if (!message.isDelivered() ) {
                notDeliveredMessage.add(message);

            }
        }
        return ResponseEntity.ok(notDeliveredMessage);
    }

    @GetMapping("/read/{id}")
    public ResponseEntity<?> readMessage(@PathVariable Long id, @RequestHeader Long userId) {
        if (!participantRepository.existsByChatIdAndUserId(id, userId)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<Message> messageList = messageService.findByChatIdAndUserIdNot(id, userId);
        Date date = new Date();

        for (Message message : messageList) {
            if (message.isRead() == false) {
                message.setRead(true);
                message.setReadTimestamp(date.getTime());
                messageService.update(message);

            }


        }
        return ResponseEntity.ok(messageService.getTenMessagesByChatId(id));
    }


    @GetMapping("/getTenMessagesByUserId/{id}")
    public ResponseEntity<?> getTenMessageByUserId(@PathVariable Long id) {
        return ResponseEntity.ok(messageService.getTenMessagesByUserId(id));
    }


    @GetMapping("/create")
    public ResponseEntity<?> createMessage(@RequestBody Message message) {
        message.setMessageType("basic");
        Date date = new Date();
        message.setCreatedTimestamp(date.getTime());
        message.setDelivered(false);
        message.setRead(false);
        messageService.add(message);
        return ResponseEntity.ok("Message successfully added");
    }

}

