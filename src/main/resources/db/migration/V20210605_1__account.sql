drop table if exists account;
create table account(
    id serial,
    email varchar(255),
    password varchar(255),
    createdDate date,
    lastLogin date
);